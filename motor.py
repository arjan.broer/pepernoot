#!/usr/bin/python

import time

import RPi.GPIO as GPIO


class Motor:
    SEQUENCE = [[1, 0, 0, 0],
                [1, 1, 0, 0],
                [0, 1, 0, 0],
                [0, 1, 1, 0],
                [0, 0, 1, 0],
                [0, 0, 1, 1],
                [0, 0, 0, 1],
                [1, 0, 0, 1]]

    def __init__(self, step_pins, steps_per_rotation):
        self.step_pins = step_pins
        self.steps_per_rotation = steps_per_rotation

        for pin in step_pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, False)

    def rotate(self, degrees):
        number_of_steps = (degrees / 360) * self.steps_per_rotation
        step_counter = 0
        count = 0
        while count < number_of_steps:
            for pin in range(0, 4):
                if self.SEQUENCE[step_counter][pin] != 0:
                    GPIO.output(self.step_pins[pin], True)
                else:
                    GPIO.output(self.step_pins[pin], False)

            step_counter += 1
            if step_counter >= len(self.SEQUENCE):
                step_counter = 0
            time.sleep(0.001)
            count += 1
        self.stop_motor()

    def stop_motor(self):
        for pin in self.step_pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, False)
