create table rfidcard (
    RFID integer primary key not null ,
    DAYLIMIT integer not null default 0,
    NAME text unique
);

create table withdraws (
    RFID integer not null,
    timestamp datetime not null,
    FOREIGN KEY (RFID) references rfid(RFID)
)