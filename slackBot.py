import configparser
import re
import time

from slackclient import SlackClient

CONFIG_GROUP = "slack"
CONFIG_FILE = "slackBot.ini"
MESSAGE_TYPE = 'message'
TYPE_FIELD = 'type'
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"
IDENTIFY_COMMAND = "^Dat was (\\w+).$"
ASSIGN_COMMAND = "^Geef (\\w+) een limiet van (\\d+).$"
GIVE_COMMAND = "Geef pepernoten!"


class SlackBot:
    def __init__(self, token, channel, command_handler):
        self.command_handler = command_handler
        self.channel = channel
        self.slack_client = SlackClient(token)
        self.slack_client.rtm_connect(with_team_state=False)
        self.client_id = self.slack_client.api_call("auth.test")["user_id"]

    def send_message_to_channel(self, message):
        self.slack_client.rtm_send_message(self.channel, message)

    def listen_for_commands(self):
        events = self.slack_client.rtm_read()
        for event in events:
            if "type" in event and event["type"] == "message":
                command = self.parse_direct_mention(event['text'])
                if command:
                    self.process_command(command)

    def parse_direct_mention(self, message_text):
        matches = re.search(MENTION_REGEX, message_text)

        if matches and matches.group(1) == self.client_id:
            return matches.group(2).strip()
        else:
            return None

    def process_command(self, command):
        identify_matches = re.search(IDENTIFY_COMMAND, command)
        assign_matches = re.search(ASSIGN_COMMAND, command)
        give_matches = re.search(GIVE_COMMAND, command)
        if identify_matches:
            self.command_handler.match_last_with(identify_matches.group(1))
        elif assign_matches:
            self.command_handler.assign_limit(assign_matches.group(1), int(assign_matches.group(2)))
        elif give_matches:
            self.command_handler.give_portion()


class Handler:
    def __init__(self):
        pass # No initialization needed

    @staticmethod
    def match_last_with(name):
        print("last identity is " + name)


if __name__ == "__main__":
    simple_handler = Handler()
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    slackConfig = config[CONFIG_GROUP]
    bot = SlackBot(slackConfig["token"], slackConfig["channel"], simple_handler)
    bot.send_message_to_channel(slackConfig['welcome-message'])
    while True:
        bot.listen_for_commands()
        time.sleep(1)
