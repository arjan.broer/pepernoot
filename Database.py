import sqlite3
from datetime import date


class PepernootRepo:
    def __init__(self, database_file="pepernoot.db"):
        self.connection = sqlite3.connect(database_file)

    def add_rfid(self, rfid):
        sql = "INSERT INTO rfidcard(rfid, daylimit) values(?, ?)"

        self.connection.execute(sql, rfid)
        self.connection.commit()

    def set_name_for_rfid(self, rfid, name):
        sql = "update rfidcard set name = ? where rfid = ?"

        self.connection.execute(sql, (name, rfid))
        self.connection.commit()

    def add_withdraw(self, withdraw):
        sql = "INSERT INTO withdraws(rfid, timestamp) values(?,?)"

        self.connection.execute(sql, withdraw)
        self.connection.commit()

    def can_withdraw(self, rfid):
        rfid_card_cursor = self.connection.cursor()
        rfid_card_cursor.execute("select rfid, daylimit from rfidcard where rfid = ?", (rfid,))
        rfid_card = rfid_card_cursor.fetchone()
        max_withdraws = 0
        if rfid_card is None:
            self.add_rfid((rfid, 0))
        else:
            max_withdraws = rfid_card[1]

        withdraws_today_cursor = self.connection.cursor()
        start_of_day = date.today()
        withdraws_today_cursor.execute("select count(*) from withdraws where rfid = ? and timestamp > ?",
                                       (rfid, start_of_day))
        withdrawals = withdraws_today_cursor.fetchone()
        return withdrawals[0] < max_withdraws

    def get_rfidcard(self, rfid):
        cursor = self.connection.cursor()
        cursor.execute("select rfid, daylimit, name from rfidcard where rfid = ?", (rfid,))
        rfid_card = cursor.fetchone()
        if rfid_card is not None:
            return int(rfid_card[0]), int(rfid_card[1]), rfid_card[2]
        else:
            return None, None, None

    def change_limit(self, name, limit):
        self.connection.execute("update rfidcard set daylimit = ? where name = ?", (limit, name))
        self.connection.commit()
