#!/usr/bin/python3
import configparser
import os
from datetime import datetime

import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522

from Database import PepernootRepo
from motor import Motor
from slackBot import SlackBot


class Pepernoot:
    def __init__(self):
        self.bot = None
        self.motor = None
        self.database = None
        self.reader = None
        self.last_id = None
        script_dir = os.path.dirname(__file__)
        config = configparser.ConfigParser()
        config.read(os.path.join(script_dir, "pepernoot.ini"))

        GPIO.setmode(GPIO.BCM)
        motor_config = config['motor']
        pin0 = int(motor_config['pin0'])
        pin1 = int(motor_config['pin1'])
        pin2 = int(motor_config['pin2'])
        pin3 = int(motor_config['pin3'])
        motor_pins = [pin0, pin1, pin2, pin3]
        self.motor = Motor(motor_pins, int(motor_config['stepsPerRevolution']))

        self.reader = SimpleMFRC522()

        self.database = PepernootRepo(config['database']['file'])

        slack_config = config['slack']
        self.bot = SlackBot(slack_config['token'], slack_config['channel'], self)
        self.bot.send_message_to_channel(slack_config['welcome-message'])

    def start(self):
        while True:
            rfid = self.reader.read_id_no_block()
            if rfid is not None:
                self.last_id = rfid
                print("read id " + str(self.last_id))
                (rfid, daylimit, name) = self.database.get_rfidcard(self.last_id)
                if name is None:
                    name = str(rfid)
                if self.database.can_withdraw(self.last_id):
                    self.motor.rotate(90)
                    self.database.add_withdraw((self.last_id, datetime.now()))
                    self.bot.send_message_to_channel(name + ' heeft net pepernoten gekregen.')
                else:
                    print("no pepernoten for you " + name)
                    self.bot.send_message_to_channel(
                        name + ' heeft al genoeg pepernoten gekregen. Morgen weer een nieuwe kans.')

            self.bot.listen_for_commands()

    def match_last_with(self, name):
        print("match " + str(self.last_id) + " with name " + name)
        if self.last_id is not None:
            self.database.set_name_for_rfid(self.last_id, name)
            self.bot.send_message_to_channel("Rfid " + str(self.last_id) + " is gekoppeld aan naam " + name)

    def assign_limit(self, name, limit):
        self.database.change_limit(name, limit)
        self.bot.send_message_to_channel(name + " kan nu iedere dag " + str(limit) + " porties pepernoten pakken.")

    def give_portion(self):
        self.motor.rotate(90)
        self.bot.send_message_to_channel("Als je blieft")


if __name__ == "__main__":
    try:
        pepernoot = Pepernoot()
        pepernoot.start()
    except KeyboardInterrupt:
        print("Stopping program because of keyboard interrupt")
    finally:
        GPIO.cleanup()
