# pepernoot

code for the pepernoten dispencer

## Setting up python with the correct libraries

``` bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install sqlite3
sudo apt-get install python3 python3-dev python3-pip
sudo pip3 install spidev
sudo pip3 install mfrc522
sudo pip3 install slackclient==1.3.2
```

## Configure the raspberry pi to enable spi interface

open the raspi-cofig interface using `sudo raspi-config` and enable spi in the interfaces menu

## connection of the RFID scanner

The RFID RC522 scanner needs to be connected to the raspberry pi zero as noted in the table below.

| RC522 pin | RC522 name | Rasp pi pin | GPIO name |
|-----------|------------|-------------|-----------|
| 1         | vcc   | 1           | +3V3       |
| 2         | RST   | 22 | BCM 25 |
| 3         | GND   | 6 | GND |
| 4         | MISO  | 21 | BCM 9 |
| 5         | MOSI  | 19 | BCM 10 |
| 6         | SCK   | 23 | BCM 11 |
| 7         | NSS   | 24 | BCM 8 |
| 8         | IRQ   | - | Not Connected |

## Connection of the RB-MOTO2 hat board

The RB-MOTO2 driver can not be connected directly to the raspberry pi using the header. There is a 
pin overlap with the RFID scanner. The RB-MOTO2 is connected as in the table below.

| RB-MOTO2 pin | Rasp pi pin | GPIO name |
|---|---|---|
| 2 | 2 | +5V |
| 6 | ? | GND |
| 11 | 11 | BCM 17 |
| 12 | 12 | BCM 18 |
| 13 | 13 | BCM 27 |
| 15 | 15 | BCM 22 |

## Database creation
The application uses a sqlite database. Using the included pepernoot_init.sql script you can initiate the database.

``` bash
sqlite3 pepernoot.db
sqlite> .read pepernoot_init.sql
```

## Application configuration
The application is configured using the `pepernoot.ini` file. Provide the followin information in the ini file. The ini 
file must be located in the same directory as the `Pepernoot.py` file.

``` ini
[database]
file=./pepernoot.db

[motor]
pin0=17
pin1=18
pin2=27
pin3=22
stepsPerRevolution=4096
```

## Configure the slackbot
you need to create a channel on slack and configure an app on that channel. The resulting bot user oauth needs to be 
included in the config file as below.
``` ini
[slack]
token=<<<oauth-code>>>
channel=<<<Channel id>>>
welcome-message=De pepernotenautomaat staat weer aan. Tot zo!
```